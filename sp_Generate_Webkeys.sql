USE [CRE]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'sp_Generate_Webkeys')
   exec('CREATE PROCEDURE [dbo].[sp_Generate_Webkeys] AS BEGIN SET NOCOUNT ON; END')
GO
ALTER PROCEDURE [dbo].[sp_Generate_Webkeys]
	@number_of_keys INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @web_keys TABLE (
      web_key UNIQUEIDENTIFIER
    );

	DECLARE @i INT;
	SET @i = 0;
	WHILE @i < @number_of_keys
	BEGIN
		INSERT INTO @web_keys VALUES ( NEWID() );
		SET @i = @i + 1;
	END

	SELECT * FROM @web_keys;

END

-- EXEC [sp_Generate_Webkeys] 10

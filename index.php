<h1>Enter the number of web keys you would like to generate:</h1>
<form>
  <input name='num' type='number' min='1'/>
  <input type='submit'/>
</form>

<?php

if(

  !empty($_GET["num"]) &&
  is_numeric($_GET["num"]) &&
  $_GET["num"] > 0

){

  $mssql_server = "localhost";
  $mssql_username = "username";
  $mssql_password = "password";
  $mssql_database = "dbname";
  $mssql_driver = "ODBC Driver 13 for SQL Server";

  if (file_exists((__DIR__) . "/credentials_local.inc.php"))
    require_once((__DIR__) . "/credentials_local.inc.php");

  $mssql_query = "EXEC [sp_Generate_Webkeys] $_GET[num]";

  // connect
  $mssql_connection = odbc_connect(
    "DRIVER={$mssql_driver};Server=$mssql_server;Database=$mssql_database;",
    $mssql_username, 
    $mssql_password
  ) or die("could not connect: " . odbc_errormsg());

  $mssql_result = odbc_exec($mssql_connection, $mssql_query) or die(odbc_errormsg());

  odbc_result_all($mssql_result, "border=1");

}

?>
